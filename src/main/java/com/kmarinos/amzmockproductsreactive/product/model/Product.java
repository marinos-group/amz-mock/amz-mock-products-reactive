package com.kmarinos.amzmockproductsreactive.product.model;

import com.kmarinos.amzmockproductsreactive.department.model.Department;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Embedded;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table("products")
public class Product {

  @Id
  @Include
  String id;
  @Column("department_id")
  Department department;
  @Embedded.Empty
  ProductRating rating;
  Double price;
  String url;
  String imgUrl;
  String descriptionShort;
  String descriptionLong;
}
