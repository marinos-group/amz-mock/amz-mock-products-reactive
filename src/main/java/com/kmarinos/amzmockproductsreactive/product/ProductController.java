package com.kmarinos.amzmockproductsreactive.product;

import com.kmarinos.amzmockproductsreactive.department.repository.DepartmentRepository;
import com.kmarinos.amzmockproductsreactive.product.model.Product;
import com.kmarinos.amzmockproductsreactive.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ProductController {

  private final ProductRepository productRepository;
  private final DepartmentRepository departmentRepository;

  @GetMapping("products")
  public Flux<Product> getAllProducts(){
      return productRepository.getSomeProducts()
//          .flatMap(c->departmentRepository.findById(c.getDepartment().getId()).map(d->{c.setDepartment(d);return c;}))
          ;
  }
}
