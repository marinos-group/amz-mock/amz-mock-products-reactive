package com.kmarinos.amzmockproductsreactive.product.repository;

import com.kmarinos.amzmockproductsreactive.product.model.Product;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ProductRepository extends ReactiveCrudRepository<Product,String> {

  @Query("select * from products limit 10000")
  public Flux<Product> getSomeProducts();
}
