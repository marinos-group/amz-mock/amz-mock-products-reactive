package com.kmarinos.amzmockproductsreactive.department.model;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table("departments")
public class Department {
  String name;
  String bestsellerUrl;
  String category;
  @Id
  @Include
  String id;
  @Exclude
  Department parent;
  @Transient
  @Default
  Map<String, Department> subDepartments = new HashMap<>();

  public Stream<Department> streamSubDepartments() {
    return Stream.concat(
        Stream.of(this),
        subDepartments.values().stream().flatMap(Department::streamSubDepartments));
  }
}
