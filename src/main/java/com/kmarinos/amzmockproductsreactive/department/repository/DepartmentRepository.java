package com.kmarinos.amzmockproductsreactive.department.repository;

import com.kmarinos.amzmockproductsreactive.department.model.Department;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface DepartmentRepository extends ReactiveCrudRepository<Department,String> {

}
